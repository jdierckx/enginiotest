// Copyright (c) 2014, Jeroen Dierckx
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//    * Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//    * Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1
import Enginio 1.0

ColumnLayout {
    EnginioClient {
        id: enginio
        backendId: backendID.text

        Component.onCompleted: {
            log.append('<b>' + qsTr("Enginio client ready") + '</b>')
            log.cursorPosition = log.length
        }

        onAuthenticationStateChanged: {
            if(enginio.authenticationState == Enginio.Authenticating) {
                //status.text = qsTr("Logging on...")
                //progress.visible = true
                //progress.value = 0

                logonButton.text = qsTr("Logging on")
                log.append('<b>' + qsTr("Authenticating") + " " + userName.text + '</b>')
                log.cursorPosition = log.length
            }
        }

        onSessionAuthenticated: {
            log.append('<b>' + qsTr("Session authentication succeeded:") + '</b>')
            log.append('<pre><code>' + JSON.stringify(reply.data) + '</code></pre>')
            log.cursorPosition = log.length

            logonButton.text = qsTr("Log off")
        }

        onSessionAuthenticationError: {
            log.append('<b style="color: red">' + qsTr("Session authentication error") + " " + reply.errorCode + ':</b>')
            log.append('<pre><code>' + reply.errorString + '</code></pre>')
            log.cursorPosition = log.length

            logonButton.text = qsTr("Log on")
        }

        onSessionTerminated: {
            log.append('<b>' + qsTr("Session terminated") + '</b>')
            log.cursorPosition = log.length

            logonButton.text = qsTr("Log on")
        }

        onFinished: {
            // Note: also called on error, but we handle that in the error callback
            if(!reply.isError) {
                log.append('<b>' + qsTr("Request succeeded:") + '</b>')
                log.append('<pre><code>' + JSON.stringify(reply.data) + '</code></pre>')
                log.cursorPosition = log.length
            }
        }

        onError: {
            log.append('<b style="color: red">' + qsTr("Request error") + " " + reply.errorCode + ':</b>')
            log.append('<pre><code>' + reply.errorString + '</code></pre>')
            log.cursorPosition = log.length
        }
    }

    EnginioOAuth2Authentication {
        id: identity
    }

GroupBox {
    Layout.fillWidth: true

    title: qsTr("Enginio Authentication")

    ColumnLayout {
        width: parent.width

        RowLayout {
            Layout.fillWidth: true

            TextField {
                id: backendID
                Layout.fillWidth: true
                placeholderText: qsTr("Enginio Backend ID")

                Keys.onReturnPressed: { userName.focus = true }
            }
        }

        RowLayout {
            Layout.fillWidth: true

            TextField {
                id: userName
                Layout.fillWidth: true
                placeholderText: qsTr("User name")

                // Enabled when ready to log on
                enabled: backendID.text.length && enginio.authenticationState !== Enginio.Authenticating && enginio.authenticationState !== Enginio.Authenticated

                Keys.onReturnPressed: { password.focus = true }
            }

            TextField {
                id: password
                Layout.fillWidth: true
                echoMode: TextInput.Password
                placeholderText: qsTr("Password")

                // Enabled when the user name field is
                enabled: userName.enabled

                Keys.onReturnPressed: { if(logonButton.enabled) logonButton.clicked() }
            }

            Button {
                id: logonButton
                text: qsTr("Log on")

                // Enabled when ready to log on
                enabled: backendID.text.length && userName.text.length && password.text.length && enginio.authenticationState !== Enginio.Authenticating

                onClicked: {
                    if(enginio.authenticationState === Enginio.NotAuthenticated || enginio.authenticationState === Enginio.AuthenticationFailure) {
                        identity.user = userName.text
                        identity.password = password.text
                        enginio.identity = identity
                    }
                    else if(enginio.authenticationState === Enginio.Authenticated) {
                        log.append('<b>' + qsTr("Logging off") + " " + identity.user + '</b>')
                        log.cursorPosition = log.length

                        enginio.identity = null
                    }
                }
            }
        }
    }
}

GroupBox {
    Layout.fillWidth: true

    title: qsTr("Request")

    ColumnLayout {
        width: parent.width

        TextArea {
            id: request
            Layout.fillWidth: true
            Layout.fillHeight: true

            textFormat: TextEdit.PlainText
        }

        RowLayout {
            Layout.fillWidth: true

            ComboBox {
                id: requestType
                implicitWidth: 100
                model: ["Query", "Update", "Create", "Remove"]
            }

            ComboBox {
                id: operation
                implicitWidth: 250
                model: ["Object Operation", "User Operation", "Usergroup Operation", "Usergroup Members Operation", "Access Control Operation", "File Operation"]
            }

            Button {
                id: requestButton
                text: qsTr("Start Request")

                // Enabled when there is a request
                enabled: backendID.text.length && request.text.length

                onClicked: {
                    try {
                        var requestValue = JSON.parse(request.text)

                        var operationMode = Enginio.ObjectOperation
                        switch(operation.currentIndex) {
                            case 0:
                                operationMode = Enginio.ObjectOperation
                                break

                            case 1:
                                operationMode = Enginio.UserOperation
                                break

                            case 2:
                                operationMode = Enginio.UsergroupOperation
                                break

                            case 3:
                                operationMode = Enginio.UsergroupMembersOperation
                                break

                            case 4:
                                operationMode = Enginio.AccessControlOperation
                                break

                            case 5:
                                operationMode = Enginio.FileOperation
                                break
                        }

                        log.append('<b>' + "Sending a " + requestType.currentText + " request as a " + operation.currentText + '</b>')
                        log.append('<pre><code>' + request.text + '</code></pre>')
                        log.cursorPosition = log.length

                        switch(requestType.currentIndex) {
                            case 0:
                                enginio.query(requestValue, operationMode)
                                break

                            case 1:
                                enginio.update(requestValue, operationMode)
                                break

                            case 2:
                                enginio.create(requestValue, operationMode)
                                break

                            case 3:
                                enginio.remove(requestValue, operationMode)
                                break
                        }

                    } catch(e) {
                        log.append('<b style="color: red">' + qsTr("Request is not valid JSON: ") + e + '</b>')
                        log.cursorPosition = log.length
                    }
                }
            }

            Button {
                id: clearLogButton
                text: qsTr("Clear log")

                onClicked: {
                    log.text = ""
                }
            }
        }
    }
}

TextArea {
    id: log
    Layout.fillWidth: true
    Layout.fillHeight: true

    readOnly: true
    textFormat: TextEdit.RichText
}

}
